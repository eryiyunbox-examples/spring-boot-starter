# Spring Boot 在 [21云盒子](https://www.21cloudbox.com/) 的启动模板

这是 [21云盒子](https://www.21cloudbox.com/) 上创建的 [Spring Boot](https://spring.io/) 的启动模板。

## 部署

详情看 [https://www.21cloudbox.com/blog/solutions/how-to-deploy-java-spring-in-produciton-server.html](https://www.21cloudbox.com/blog/solutions/how-to-deploy-java-spring-in-produciton-server.html)